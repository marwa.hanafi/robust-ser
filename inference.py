import subprocess
import sys

def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])
install("datasets")

#importing necessary packages
import torchaudio.functional as F
import torch
import torchaudio.transforms as T
import numpy as np
import os
import soundfile as sf
import random
import time
from tqdm import tqdm
from transformers import Wav2Vec2FeatureExtractor
from datasets import Dataset
from transformers import HubertForSequenceClassification
import pandas as pd
from datasets import Dataset
from torch.utils.data import DataLoader
import librosa
import matplotlib.pyplot as plt


if torch.cuda.is_available():
    # Set the device to GPU
    device = torch.device("cuda")
    print("GPU available:", torch.cuda.get_device_name(0))  # Print the GPU name
else:
    print("No GPU available, using CPU instead.")
    device = torch.device("cpu")


#function needed to run the inference_function
def map_to_array(example):
    speech, _ = librosa.load(example["path"], sr=16000, mono=True)
    example["speech"] = speech
    return example

# Will be needed to compute accuracy
def predict(outputs):
    probabilities = torch.softmax(outputs["logits"], dim=1)
    predictions = torch.argmax(probabilities, dim=1)
    return predictions

#dataset class
class EmotionDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        pattern = {1:0, 2:0, 3:1, 4:3, 5:2}
        self.labels = [pattern[x] for x in labels]

    def __getitem__(self, idx):
        item = {key: val[idx] for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)
    
    


#2nd, write a function that does all the preprocessing steps and computes accuracy
def model_inference(path_to_db, model_path):
  #load the trained model
  model = HubertForSequenceClassification.from_pretrained("superb/hubert-large-superb-er")
  model.load_state_dict(torch.load(model_path))  #if on gpu uncomment this, comment the line after
  #model.load_state_dict(torch.load(model_path, map_location=torch.device('cpu'))) #if on cpu

  model.to(device)
  RAV = path_to_db
  dir_list = ['Actor_20', 'Actor_21', 'Actor_22', 'Actor_23', 'Actor_24']
  emotion = []
  gender = []
  path = []
  for i in dir_list:
      fname = os.listdir(RAV + i)
      for f in fname:
          part = f.split('.')[0].split('-')
          emotion.append(int(part[2]))
          temp = int(part[6])
          if temp%2 == 0:
              temp = "female"
          else:
              temp = "male"
          gender.append(temp)
          path.append(RAV + i + '/' + f)

  RAV_df = pd.DataFrame(emotion)
  RAV_df = RAV_df.replace({1:'neutral', 2:'neutral', 3:'happy', 4:'sad', 5:'angry', 6:'fear', 7:'disgust', 8:'surprise'})
  RAV_df = pd.concat([pd.DataFrame(gender), RAV_df, pd.DataFrame(emotion)],axis=1)
  RAV_df.columns = ['gender','emotion','labels']
  RAV_df['source'] = 'RAVDESS'
  RAV_df = pd.concat([RAV_df,pd.DataFrame(path, columns = ['path'])],axis=1)
  #use only 4 emotions
  RAV_df = RAV_df[(RAV_df["emotion"]=="neutral") | (RAV_df["emotion"]=="happy") | (RAV_df["emotion"]=="sad") | (RAV_df["emotion"]=="angry")]
  #Tokenization
  inference_data = Dataset.from_pandas(RAV_df).map(map_to_array)

  feature_extractor = Wav2Vec2FeatureExtractor.from_pretrained("superb/hubert-large-superb-er")

  inference_encodings = feature_extractor(list(inference_data["speech"]), sampling_rate=16000, padding=True, return_tensors="pt")
  #turn data into a dataset object
  inference_dataset = EmotionDataset(inference_encodings, list(inference_data["labels"]))

  #inference
  # Get test data by DataLoader
  inference_loader = DataLoader(inference_dataset, batch_size=1, shuffle=False)

  # Start testing
  model.eval()

  with torch.no_grad():

      correct = 0
      count = 0
      record = {"labels":list(), "predictions":list()}

      pbar = tqdm(inference_loader)
      for batch in pbar:
          input_ids = batch['input_values'].to(device)
          attention_mask = batch['attention_mask'].to(device)
          labels = batch['labels'].to(device)
          outputs = model(input_ids, attention_mask=attention_mask, labels=labels)
          loss = outputs['loss']

          # make predictions
          predictions = predict(outputs)

          # count accuracy
          correct += predictions.eq(labels).sum().item()
          count += len(labels)
          accuracy = correct * 1.0 / count

          # show progress along with metrics
          pbar.set_postfix({
              'loss': '{:.3f}'.format(loss.item()),
              'accuracy': '{:.3f}'.format(accuracy)
          })

          # record the results
          record["labels"] += labels.cpu().numpy().tolist()
          record["predictions"] += predictions.cpu().numpy().tolist()

      pbar.close()

  time.sleep(0.3)
  print("The final accuracy on the test dataset: %s%%" % round(accuracy*100,4))