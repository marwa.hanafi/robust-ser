import torchaudio
import torchaudio.functional as F
import torch
import torchaudio.transforms as T
import numpy as np
import os
import soundfile as sf
import random
import pandas as pd

#Function that does the mix at 16khz
def signal_noise_mix (signal_path, noise_path, snr_list):
  speech, original_sample_rate = torchaudio.load(signal_path)
  #resample ravdess to 16k
  resampler = T.Resample(original_sample_rate, 16000, dtype=speech.dtype)
  resampled_speech = resampler(speech)
  noise, _ = torchaudio.load(noise_path)
  print(noise.shape)
  #cut noise to match shape of clean speech
  noise = noise[:, : resampled_speech.shape[1]]
  #mixing
  noisy_speeches = F.add_noise(resampled_speech, noise, snr_list)
  return noisy_speeches

#choosing the noises we'll be working with
filtered_folders = ['OMEETING_16k', 'OHALLWAY_16k', 'DKITCHEN_16k','OOFFICE_16k', 'DLIVING_16k','PCAFETER_16k']
def generate_df(snr_levels_list, dataset_path, demand_path, output_path):
    #fixer les seeeeds
    random.seed(42)
    np.random.seed(42)
    torch.manual_seed(42)
    # Convert list to tensor (to match previous function)
    snr_levels = torch.tensor(snr_levels_list)
    list_of_dicos = []
    # Iterate over ravdess
    for actor_folder in os.listdir(dataset_path):
        actor_folder_path = os.path.join(dataset_path, actor_folder)
        if os.path.isdir(actor_folder_path):
            # Extract actor number from actor_folder
            actor_number = actor_folder.split('_')[1]
            # Iterate through each recording in the actor's folder
            for audio_file in os.listdir(actor_folder_path):
              audio_file_path = os.path.join(dataset_path, actor_folder, audio_file)
              # Pick a noise from DEMAND
              folder_index = int(np.random.uniform(0, len(filtered_folders)))
              noise_category = filtered_folders[folder_index]
              extracted_name = noise_category.split('_')[0]

              # Select a random noise audio from the chosen category
              noise_subdir_path = os.path.join(demand_path, noise_category, extracted_name)
              noise_file_index = int(np.random.uniform(0, len(os.listdir(noise_subdir_path))))
              noise_file = os.listdir(noise_subdir_path)[noise_file_index]
              noise_file_path = os.path.join(noise_subdir_path, noise_file)

              # Mixing audio
              noised_audios = signal_noise_mix(audio_file_path, noise_file_path, snr_levels)
              # Save each noisy speech
              for i, snr_val in enumerate(snr_levels_list):
                  snr_output_path = os.path.join(output_path, f"SNR_{snr_val}")
                  os.makedirs(snr_output_path, exist_ok=True)
                  # Create actor folder within SNR output path
                  actor_output_path = os.path.join(snr_output_path, f"Actor_{actor_number}")
                  os.makedirs(actor_output_path, exist_ok=True)
                  small_dico = {
                            'SNR': snr_val,
                            'actor': actor_number,
                            'RAVDESS_file': audio_file,
                            'Noise_Type': extracted_name,
                            'Noise_file': noise_file,
                        }
                  #filename = os.path.basename(audio_file_path).split('.')[0] + f'_SNR{snr_val}.wav'
                  filename = os.path.basename(audio_file_path).split('.')[0] + '.wav'
                  path = os.path.join(actor_output_path, filename)
                  small_dico['Noised_Ravdess_path'] = path
                  list_of_dicos.append(small_dico)
                  # Convert tensor to numpy array
                  audio_data = noised_audios[i].numpy()
                  # Ensure audio data is in the range [-1, 1]
                  #audio_data = np.clip(audio_data, -1, 1)
                  # Save audio using soundfile
                  sf.write(path, audio_data, samplerate = 16000)
    return list_of_dicos