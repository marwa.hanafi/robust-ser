import subprocess
import sys
#install a package not available by default in colab
def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])
install("datasets")


#importing necessary packages
import os
import pandas as pd
from transformers import Wav2Vec2FeatureExtractor
from datasets import Dataset
import librosa
import subprocess
import sys
import torch
from transformers import HubertForSequenceClassification
from torch.optim import AdamW
from torch.utils.data import DataLoader
import numpy as np
import matplotlib.pyplot as plt
import time
from tqdm import tqdm

if torch.cuda.is_available():
    # Set the device to GPU
    device = torch.device("cuda")
    print("GPU available:", torch.cuda.get_device_name(0))  # Print the GPU name
else:
    print("No GPU available, using CPU instead.")
    device = torch.device("cpu")

def map_to_array(example):
    speech, _ = librosa.load(example["path"], sr=16000, mono=True)
    example["speech"] = speech
    return example

# Prediction function
def predict(outputs):
    probabilities = torch.softmax(outputs["logits"], dim=1)
    predictions = torch.argmax(probabilities, dim=1)
    return predictions


class EmotionDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        pattern = {1:0, 2:0, 3:1, 4:3, 5:2}
        self.labels = [pattern[x] for x in labels]

    def __getitem__(self, idx):
        item = {key: val[idx] for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)

def train_model(dataset_path, epoch, model_path): #epoch = number of epochs
    
    RAV = dataset_path
    dir_list = os.listdir(RAV)
    dir_list.sort()
    dir_list.pop()
    emotion = []
    gender = []
    path = []
    for i in dir_list:
        fname = os.listdir(RAV + i)
        for f in fname:
            part = f.split('.')[0].split('-')
            emotion.append(int(part[2]))
            temp = int(part[6])
            if temp%2 == 0:
                temp = "female"
            else:
                temp = "male"
            gender.append(temp)
            path.append(RAV + i + '/' + f)

    RAV_df = pd.DataFrame(emotion)
    RAV_df = RAV_df.replace({1:'neutral', 2:'neutral', 3:'happy', 4:'sad', 5:'angry', 6:'fear', 7:'disgust', 8:'surprise'})
    RAV_df = pd.concat([pd.DataFrame(gender), RAV_df, pd.DataFrame(emotion)],axis=1)
    RAV_df.columns = ['gender','emotion','labels']
    RAV_df['source'] = 'RAVDESS'
    RAV_df = pd.concat([RAV_df,pd.DataFrame(path, columns = ['path'])],axis=1)
    RAV_df.emotion.value_counts()
    RAV_df = RAV_df[(RAV_df["emotion"]=="neutral") | (RAV_df["emotion"]=="happy") | (RAV_df["emotion"]=="sad") | (RAV_df["emotion"]=="angry")]
    train_df = RAV_df[RAV_df['path'].str.contains('Actor_0[1-9]|Actor_1[0-9]')]
    test_df = RAV_df[~RAV_df['path'].str.contains('Actor_0[1-9]|Actor_1[0-9]')]
    train_data = Dataset.from_pandas(train_df).map(map_to_array)
    test_data = Dataset.from_pandas(test_df).map(map_to_array)

    feature_extractor = Wav2Vec2FeatureExtractor.from_pretrained("superb/hubert-large-superb-er")

    train_encodings = feature_extractor(list(train_data["speech"]), sampling_rate=16000, padding=True, return_tensors="pt")
    test_encodings = feature_extractor(list(test_data["speech"]), sampling_rate=16000, padding=True, return_tensors="pt")
    train_dataset = EmotionDataset(train_encodings, list(train_data["labels"]))
    test_dataset = EmotionDataset(test_encodings, list(test_data["labels"]))
    # Loading the model
    model = HubertForSequenceClassification.from_pretrained("superb/hubert-large-superb-er")
    model.to(device)

    # Loading the optimizer
    optim = AdamW(model.parameters(), lr=1e-5)
    # Start training
    model.train()

    train_loss = list()
    train_accuracies = list()
    for epoch_i in range(epoch):
        print('Epoch %s/%s' % (epoch_i + 1, epoch))
        time.sleep(0.3)

        # Get training data by DataLoader
        train_loader = DataLoader(train_dataset, batch_size=2, shuffle=True)

        correct = 0
        count = 0
        epoch_loss = list()

        pbar = tqdm(train_loader)
        for batch in pbar:
            optim.zero_grad()
            input_ids = batch['input_values'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['labels'].to(device)
            outputs = model(input_ids, attention_mask=attention_mask, labels=labels)
            loss = outputs['loss']
            loss.backward()
            optim.step()

            # make predictions
            predictions = predict(outputs)

            # count accuracy
            correct += predictions.eq(labels).sum().item()
            count += len(labels)
            accuracy = correct * 1.0 / count

            # show progress along with metrics
            pbar.set_postfix({
                'Loss': '{:.3f}'.format(loss.item()),
                'Accuracy': '{:.3f}'.format(accuracy)
            })

            # record the loss for each batch
            epoch_loss.append(loss.item())

        pbar.close()

        # record the loss and accuracy for each epoch
        train_loss += epoch_loss
        train_accuracies.append(accuracy)
        # Plot Epoch vs Training Accuracy
    acc_X = np.arange(len(train_accuracies))+1
    plt.plot(acc_X, train_accuracies,"-", label="Training Accuracy")
    plt.xticks(acc_X)
    plt.xlabel("Epoch")
    plt.ylabel("Accuracy")
    plt.title("Epoch vs Training Accuracy")
    plt.legend()
    plt.show()
    torch.save(model.state_dict(), model_path)